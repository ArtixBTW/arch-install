#!/bin/sh
# Ask for user
echo 'Please enter wanted name of user\n'
read wanuser

# Create user
useradd -m -G wheel -s /bin/bash $wanuser
echo 'Created user'

# Set passwd
echo 'Set user password'
passwd $wanuser

echo '\nSudoers file is already configured by Artix'
