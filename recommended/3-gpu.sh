#!/bin/sh
# Check for root
if [ "$(whoami)" != root ]; then
    echo Please run this script as root or using sudo
    exit
fi

# List GPU and ask for GPU
echo 'Listing GPU info'
glxinfo -B | grep Device | sed 's|^[ \t]*||g'
printf "amd\nati\nnvidia\n"

echo 'Please type your GPU'
read -r gpu

case "$gpu" in
  *'amd'*) pacman -S xf86-video-amdgpu mesa lib32-mesa --noconfirm ;;
  *'ati'*) pacman -S xf86-video-ati mesa lib32-mesa --noconfirm ;;
  *'nvidia'*) pacman -S xf86-video-nouveau mesa lib32-mesa --noconfirm ;;
esac
