#!/bin/sh
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen

echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# Hostname
echo 'Enter hostname'
read host

# Setup /etc/hosts
echo "$host" > /etc/hostname

# Setup /etc/hosts
echo -e "# Static table lookup for hostnames.\n# See hosts(5) for details.\n$host\n127.0.0.1          localhost\n::1                localhost\n127.0.1.1  $host    $host" > /etc/hosts

# Success
echo -e '\nHostname set\n'
